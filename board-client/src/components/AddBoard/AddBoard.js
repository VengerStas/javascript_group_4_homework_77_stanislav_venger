import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {fetchPostBoard} from "../../store/actions";
import {connect} from "react-redux";

import './AddBoard.css';

class AddBoard extends Component {
    state = {
        author: '',
        message: '',
        image: null
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });
        this.props.addBoard(formData);
        this.setState ({author: '', message: ''});
    };

    changeValue = event => {
        this.setState ({
            [event.target.name]: event.target.value
        });
    };

    changeImage = event => {
        this.setState ({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <div className="board-form">
                <Form className="form">
                    <FormGroup>
                        <Label for="board-author">Author</Label>
                        <Input type="text" name="author" id="board-author"
                               value={this.state.author} onChange={this.changeValue} placeholder="Author Name" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="board-message">Message</Label>
                        <Input type="text" name="message" id="board-message"
                               value={this.state.message} onChange={this.changeValue} placeholder="Your message" required/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="board-file">File</Label>
                        <Input type="file" name="image" id="board-file" onChange={this.changeImage} />
                    </FormGroup>
                    <Button type="submit" onClick={(event) => this.submitFormHandler(event)}>Send</Button>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    state: state
});

const mapDispatchToProps = dispatch => ({
    addBoard: data => dispatch(fetchPostBoard(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddBoard);
