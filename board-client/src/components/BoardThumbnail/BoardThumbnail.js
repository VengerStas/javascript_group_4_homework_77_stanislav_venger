import React from 'react';
import {CardImg} from "reactstrap";

import './BoardThumbnail.css';

const BoardThumbnail = props => {
    if (props.image !== "null") {
        let image = 'http://localhost:8000/uploads/' + props.image;
        return <CardImg src={image} className="img-thumbnail" alt="Board Image" />;
    }
    return null;
};

export default BoardThumbnail;
