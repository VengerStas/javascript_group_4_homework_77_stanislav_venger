import React, {Component} from 'react';
import AddBoard from "../../components/AddBoard/AddBoard";
import {fetchGetBoard} from "../../store/actions";
import {connect} from "react-redux";
import {Card, CardBody, CardText, CardTitle} from "reactstrap";
import BoardThumbnail from "../../components/BoardThumbnail/BoardThumbnail";

import './Board.css';

class Board extends Component {
    componentDidMount() {
        this.props.getBoards();
    }
    render() {
        let singleBoard = this.props.board.map(board => (
           <Card key={board.id} className="board-card">
               <CardBody>
                   <BoardThumbnail image={board.image}/>
                   <CardTitle><strong>Name: </strong>{board.author}</CardTitle>
                   <CardText><strong>Message: </strong>{board.message}</CardText>
               </CardBody>
           </Card>
        ));
        return (
            <div>
                <AddBoard/>
                <div className="single-board">
                    {singleBoard}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    board: state.board
});

const mapDispatchToProps = dispatch => ({
    getBoards: () => dispatch(fetchGetBoard())
});

export default connect(mapStateToProps, mapDispatchToProps)(Board);
