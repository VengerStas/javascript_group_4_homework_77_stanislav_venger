import {BOARD_REQUEST_SUCCESS} from "./actions";

const initialState = {
    board: [],

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case BOARD_REQUEST_SUCCESS:
            return {
                ...state,
                board: action.board
            };
        default:
            return state;
    }
};


export default reducer;
