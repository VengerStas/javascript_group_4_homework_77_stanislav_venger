import axios from '../axios-Base';

export const BOARD_REQUEST ="BOARD_REQUEST";
export const BOARD_REQUEST_SUCCESS = "BOARD_REQUEST_SUCCESS";
export const BOARD_FAILURE = "BOARD_FAILURE";


export const boardRequest = () => {
    return {type: BOARD_REQUEST};
};

export const boardSuccess = board => {
    return {type: BOARD_REQUEST_SUCCESS, board};
};

export const boardFailure = error => {
    return {type: BOARD_FAILURE, error};
};

export const fetchGetBoard = () => {
    return (dispatch) => {
        dispatch(boardRequest());
        return axios.get('/board').then(response => {
            dispatch(boardSuccess(response.data));
        }, error => {
            dispatch(boardFailure(error));
        })
    }
};

export const fetchPostBoard = data => {
    return (dispatch) => {
        dispatch(boardRequest());
        axios.post('/board', data).then(() => {
            dispatch(boardRequest());
            dispatch(fetchGetBoard());
        }, error => {
            dispatch(boardFailure(error));
        })
    }
};
