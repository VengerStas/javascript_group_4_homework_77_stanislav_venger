const express = require('express');
const multer  = require ('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const db = require('./fileDb');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    res.send(db.getItem())
});

router.post('/', upload.single('image'), (req, res) => {
    const board = req.body;
    req.body.author = req.body.author || "Anonymous";
    board.id = nanoid();

    if  (req.file) {
        board.image = req.file.filename;
    }

    if (req.body.message !== '') {
        db.addItem(board);
        res.send({message: 'OK'})
    } else {
        res.status(400).send({message: "Field message is required!"})
    }
});


module.exports = router;
