const express = require('express');
const db = require('./app/fileDb');
const cors = require('cors');
const board = require('./app/board');

db.init();

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

app.use('/board', board);

app.listen(port, () => {
    console.log(port)
});
